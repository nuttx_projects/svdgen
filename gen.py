#!/usr/bin/python3
from cmsis_svd.parser import SVDParser
import sys, getopt
from tabulate import tabulate
import re

prefix = ''

def print_help():
    print('gen { -v <vendor> -d <device> | -f <svdfile> } -p <peripheral> [ -x <macro prefix> ]')

def define(macro, value, description = ""):
    description = re.sub(r'\n\s*', ' ', description)
    return ["#define " + prefix + "_" + macro, value + ("  /* " + description + " */" if description else "")]

def generate_definitions(peripherals):
    print("/* Register offsets *********************************************************/\n")

    peripheral = peripherals[0]
    definitions = []
    for register in peripheral.registers:
        register_name = register.name.replace("[","").replace("]","")
        definitions.append(define(peripheral.group_name + "_" + register_name + "_OFFSET", "0x%06x" % register.address_offset, register.description))
    print(tabulate(definitions, tablefmt="plain"))

    print("\n/* Register definitions *****************************************************/\n")
    definitions = []
    for peripheral in peripherals:
        for register in peripheral.registers:
            register_name = register.name.replace("[","").replace("]","")
            definitions.append(define(peripheral.name + "_" + register_name, "(" + prefix + "_" + peripheral.name + "_BASE + " + 
                                                      prefix + "_" + peripheral.group_name + "_" + register_name + "_OFFSET)"))
        definitions.append(["","",""])
    print(tabulate(definitions, tablefmt="plain"))

    print("\n/* Register bit definitions *************************************************/\n")
    peripheral = peripherals[0]
    definitions = []
    for register in peripheral.registers:
        register_name = register.name.replace("[","").replace("]","")
        name_prefix = peripheral.group_name + "_" + register_name

        if (len(register.fields) > 1 or (len(register.fields) > 0 and register.fields[0].bit_width != peripheral.size)):
            definitions.append([])

            for field in register.fields:
                field_name = name_prefix + ("_" + field.name if field.name != register_name else "")

                if (field.bit_width != 1):
                    mask_bits = 2 ** field.bit_width - 1 
                    if (field.bit_offset != 0):
                        shift_name = name_prefix + "_" + field.name + "_SHIFT"
                        shift_value = "(%i)" % field.bit_offset
                        definitions.append(define(shift_name, shift_value, field.description))
                        shift_name_final = prefix + "_" + shift_name
                        mask = "(0x%02x << %s)" % (mask_bits, shift_name_final)
                    else:
                        shift_name = "%i" % (field.bit_offset)
                        shift_name_final = shift_name
                        mask = "(0x%02x)" % (mask_bits)

                    mask_description = (field.description if (field.bit_offset == 0 and not field.enumerated_values) else "")
                    definitions.append(define(field_name + "_MASK", mask, mask_description))

                    if (field.enumerated_values):
                        for enum_value in field.enumerated_values:
                            enum_value_name = field_name + "_" + enum_value.name.upper()
                            if (field.bit_offset != 0):
                                enum_value_value = "(0x%x << %s)" % (enum_value.value, shift_name_final)
                            else:
                                enum_value_value = "(0x%x)" % (enum_value.value)
                            definitions.append(define(enum_value_name, enum_value_value, enum_value.description))
                else:
                    if (field.enumerated_values):
                        for enum_value in field.enumerated_values:
                            if (enum_value.value == 1):
                                enum_value_name = field_name
                                enum_value_value = "(1 << %i)" % (field.bit_offset)
                                definitions.append(define(enum_value_name, enum_value_value, enum_value.description))
                                break
                    else:
                        field_value = "(1 << %i)" % (field.bit_offset)
                        definitions.append(define(field_name, field_value, field.description))

    if (len(definitions) > 0):
        print(tabulate(definitions, tablefmt = "plain"))

def generate_map(peripherals):
    definitions = []
    for peripheral in peripherals:
        definitions.append(define(peripheral.name + "_BASE", "0x%06x" % peripheral.base_address, peripheral.description))
    print(tabulate(definitions, tablefmt="plain"))

svdfile = None
vendor = None
device = None
peripheral_name = None

try:
    opts, args = getopt.getopt(sys.argv[1:], "hv:d:p:x:f:")
except getopt.GetoptError:
    print_help()
    sys.exit(2)
for opt, arg in opts:
    if (opt == "-h"):
        print_help()
        sys.exit()
    elif opt == "-v":
        vendor = arg
    elif opt == "-d":
        device = arg
    elif opt == "-p":
        peripheral_name = arg
    elif opt == "-x":
        prefix = arg
    elif opt == "-f":
        svdfile = arg

try:
    if ((svdfile and (vendor or device)) or (not svdfile and not vendor and not device)):
        raise RuntimeError("Please specify either -f or -d and -v")
    elif ((vendor and not device) or (not vendor and device)):
        raise RuntimeError("Please specify both -d and -v")
    elif (not peripheral_name):
        raise RuntimeError("Please specify peripheral")
except RuntimeError as err:
    print("error: " + str(err))
    sys.exit(2)

if (svdfile):
    parser = SVDParser.for_xml_file(svdfile)
else:
    parser = SVDParser.for_packaged_svd(vendor, device + ".svd")

device = parser.get_device()

if (peripheral_name == "?"):
    for peripheral in device.peripherals:
        print("%s\t0x%08x\t(%s)" % (peripheral.name, peripheral.base_address, peripheral.group_name))
elif (peripheral_name == "map"):
    generate_map(device.peripherals)
else:
    peripherals = [peripheral for peripheral in parser.get_device().peripherals if peripheral.group_name == peripheral_name]
    generate_definitions(peripherals)
